import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Navigator,
    TouchableHighlight
} from 'react-native';

import Accordion from 'react-native-collapsible/Accordion';


class AccordionElements extends React.Component {
    _renderHeader(item) {
        return (
            <View style={{flexDirection: 'row',  backgroundColor: '#FFF', padding: 10, borderBottomWidth: 1, borderColor: '#ebebeb'}} >
                <View style={{backgroundColor: '#f3f1ef', width: '18%', maxWidth: 70, paddingTop: 3, paddingBottom: 3, marginRight: 10}}>
                    <Text style={{fontSize:14, textAlign:'center'}}>{item.for}</Text>
                </View>
                <View style={{flex: 8}}>
                    <Text style={{fontSize:16}} >{item.name}</Text>
                </View>
                <View style={{backgroundColor: '#f3f1ef', paddingTop: 3, paddingBottom: 3, paddingLeft: 10, paddingRight:10, marginRight: 10}}>
                    <Text style={{fontSize:14}}>{item.bars}</Text>
                </View>
            </View>
        );
    }

    _renderContent(item) {
        return (
            <View style={{flexDirection: 'row',  backgroundColor: '#f7f7f7', paddingLeft: 10, paddingTop: 3, paddingRight: 10, paddingBottom: 10}} >
                <Text style={{fontSize:14}}>{item.description}</Text>
            </View>
        );
    }

    render() {
        return (
            <Accordion
                sections={this.props.items}
                renderHeader={this._renderHeader}
                renderContent={this._renderContent}
            />
        );
    }
}

export default AccordionElements;
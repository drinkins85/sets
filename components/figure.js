import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Navigator
} from 'react-native';

import {
    StackNavigator,
} from 'react-navigation';

import AccordionElements from './accordion';

class Figure extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        title: `${navigation.state.params.setname} - fig. ${navigation.state.params.figure.number}`,
        headerStyle: {backgroundColor: '#7BC060'}
    });

    render() {
        const { params } = this.props.navigation.state;
        return (
            <View>
                <View style={{flexDirection: 'row',  backgroundColor: '#ffeec4'}} >
                    <View style={{padding: 10, backgroundColor: '#f9d8b1'} }  >
                        <Text style={{fontSize:16}}>{`fig. ${params.figure.number}`}</Text>
                    </View>
                    <View style={{padding: 10, flex: 3} }  >
                        <Text style={{fontSize:16}}>{params.figure.name}</Text>
                    </View>
                    <View style={{padding: 10, backgroundColor: '#f9d8b1', width: '20%'} }  >
                        <Text style={{fontSize:14}}>{params.figure.tune}</Text>
                    </View>
                    <View style={{padding: 10, width: '15%'} }  >
                        <Text style={{fontSize:14}}>{params.figure.bars}</Text>
                    </View>
                </View>
                <View>
                    <AccordionElements items={params.figure.elements}/>
                </View>
            </View>

        );
    }
}

export default Figure;

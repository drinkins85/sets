import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View
} from 'react-native';

import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as setsActions from '../actions/setsActions';

import SetList from './setList';

class Index extends React.Component {

    constructor(props) {
        super(props);

        this.props.setsActions.loadData();
    }

    render(){
        return(
                <SetList sets={this.props.sets} nav={this.props.nav} />
        )
    }

}

function mapStateToProps (state) {
    return {
        sets: state.sets,
        config: state.config
    }
}

function mapDispatchToProps(dispatch) {
    return {
        setsActions: bindActionCreators(setsActions, dispatch),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Index);
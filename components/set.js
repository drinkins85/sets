import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Navigator,
    Button,
    TouchableHighlight
} from 'react-native';

import {
    StackNavigator,
} from 'react-navigation';


class Set extends React.Component{

    static navigationOptions = ({ navigation }) => ({
        title: navigation.state.params.set.name,
        headerStyle: {backgroundColor: '#7BC060'}
    });

    render() {
        const { params } = this.props.navigation.state;
        const { navigate } = this.props.navigation;
        return (
            <View>
                {
                    params.set.figures.map((figure, item) => {
                        return (
                            <TouchableHighlight style={{borderBottomWidth: 1, borderColor: '#7A7A72', backgroundColor: '#FFF'}}
                                                onPress={() => navigate('Figure', {figure: figure, setname: params.set.name } )}
                                                key={item}
                                                underlayColor="#7BC060"
                            >
                                <View style={{flexDirection: 'row'}} >
                                    <View style={{padding: 10, backgroundColor: '#e3e3db'} }  >
                                        <Text style={{fontSize:16}}>{`fig. ${figure.number}`}</Text>
                                    </View>
                                    <View style={{padding: 10, flex: 3} }  >
                                        <Text style={{fontSize:16}}>{figure.name}</Text>
                                    </View>
                                    <View style={{padding: 10, backgroundColor: '#f9f9f1', width: '20%'} }  >
                                        <Text style={{fontSize:14}}>{figure.tune}</Text>
                                    </View>
                                    <View style={{padding: 10, backgroundColor: '#FFF', width: '15%'} }  >
                                        <Text style={{fontSize:14}}>{figure.bars}</Text>
                                    </View>
                                </View>
                            </TouchableHighlight>
                        )
                    })
                }
            </View>
        );
    }
}

export default Set;

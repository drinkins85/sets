import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    View,
    Navigator,
    Button,
    TouchableHighlight
} from 'react-native';

import {
    StackNavigator,
} from 'react-navigation';

import AlphabetListView from 'react-native-alphabetlistview';


 class SectionHeader extends React.Component {
     render() {
         let textStyle = {
             textAlign:'left',
             color:'#fff',
             fontWeight:'700',
             fontSize:16
         };

         let viewStyle = {
             backgroundColor: '#FEB40A',
             paddingLeft: 10
         };
         return (
             <View style={viewStyle} >
                <Text style={textStyle}>{this.props.title}</Text>
             </View>
         );
     }
 }

 class SectionItem extends React.Component {
     render() {
         return (
             <Text >{this.props.title}</Text>
         );
     }
 }

 class Cell extends React.Component {
     render() {
         return (
             <TouchableHighlight underlayColor="#7BC060"
                                 style={{borderBottomWidth: 1, borderColor: '#ebebeb', backgroundColor: '#FFF'}}
                                 onPress={() => this.props.nav('Set', { set: this.props.item })}
                                 key={this.props.item.id}
             >
                 <View style={{flexDirection: 'row', padding: 0, alignItems: 'stretch', padding: 10} }  >
                         <Text style={{fontSize:16}}>{this.props.item.name}</Text>
                 </View>
             </TouchableHighlight>
         );
     }
 }



 class SetList extends React.Component{

     getSorted(){
         if (this.props.sets.length > 0){
             let sets = this.props.sets;
             let sortedSets = {};

             for (let i=0; i<sets.length; i++ ){
                 let firstLetter = sets[i].name[0];
                 if (typeof sortedSets[firstLetter] === 'undefined'){
                     sortedSets[firstLetter] = [sets[i]]
                 } else {
                     sortedSets[firstLetter].push(sets[i])
                 }
             }
             return sortedSets;
         }
         return {};
     }

     render(){
         const  navigate  = this.props.nav;
         return(
         this.props.sets.length === 0 ?
             <View>
                 <Text>Loading</Text>
             </View>
         :
             <AlphabetListView
                 data={this.getSorted()}
                 cell={Cell}
                 cellHeight={30}
                 sectionListItem={SectionItem}
                 sectionHeader={SectionHeader}
                 sectionHeaderHeight={22.5}
                 cellProps = {{nav: navigate}}
             />
         )
     }
 }
 export default SetList;


/*

 <AlphabetListView
 data={this.getSorted()}
 cell={Cell}
 cellHeight={30}
 sectionListItem={SectionItem}
 sectionHeader={SectionHeader}
 sectionHeaderHeight={22.5}
 cellProps = {{nav: navigate}}
 />
 */



//  <Text>-{JSON.stringify(this.props.navigation)}</Text>
/*

 <View>
 <Text>{JSON.stringify(this.getSorted())}</Text>
 <Button onPress={() => navigate('Set')} title="Set" />
 </View>
 */
/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator
} from 'react-native';

import configureStore from './store/configureStore';
import {Provider} from 'react-redux';
import Index from './components/index'
import { StackNavigator } from 'react-navigation';

import Set from './components/set';
import Figure from './components/figure';

const store = configureStore();


export default class Home extends React.Component {
    static navigationOptions = {
        title: 'SetList',
        headerStyle: {backgroundColor: '#7BC060'}
    };

    render() {
        const { navigate } = this.props.navigation;
        return (
              <Provider store={store} >
                  <Index nav={navigate}/>
              </Provider>
        );
  }
}
// <Index nav={this.props.navigation}/>
const Sets = StackNavigator({
    Home: { screen: Home },
    Set: {screen: Set},
    Figure: {screen: Figure}
});

AppRegistry.registerComponent('Sets', () => Sets);

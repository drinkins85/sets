const initialState = [];

export default function sets(state = initialState, action){
    switch (action.type) {
        case 'LOAD_SETS_DATA_SUCCESS':
            return action.payload;
        default:
            return state;
    }
}